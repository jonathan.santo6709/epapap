<?php
class Indicadores extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    //MODELO CLIENTE
    $this->load->model("cliente");
    $this->load->model("usuario");
    $this->load->model("sector");
    $this->load->model("cuenta");
    $this->load->model("lectura");

    //validando si alguien esta conectado
    if ($this->session->userdata('c0nectadoUTC')) {
      //SI ESTA CONECTADO
      // code...
    } else {
      redirect('seguridades/formularioLogin');
    }
  }

  public function index()
  {
    $this->load->view("header");
    $this->load->view("indicadores/index");
    $this->load->view("footer");
  }
  public function perfil()
  {
    $data["listadoUsuarios"] = $this->usuario->consultarTodos();
    $data["listadoSectores"] = $this->sector->consultarTodos();
    $this->load->view("header");
    $this->load->view("indicadores/perfil", $data);
    $this->load->view("footer");
  }
  public function usuarios()
  {
    $var['usuarios_activos'] = $this->usuario->obtenerUsuariosPorEstadoModal("1");
    $var['usuarios_inactivos'] = $this->usuario->obtenerUsuariosPorEstadoModal("0");
    $this->load->view("indicadores/usuarios", $var);
  }

  public function clientes()
  {
    $var['clientes_activos'] = $this->cliente->obtenerClientesPorEstadoModal("ACTIVO");
    $var['clientes_inactivos'] = $this->cliente->obtenerClientesPorEstadoModal("INACTIVO");
    $this->load->view("indicadores/clientes", $var);
  }
  public function cuentas()
  {
    $var['cuentas_activos'] = $this->cuenta->obtenerCuentasPorEstadoModal("ACTIVA");
    $var['cuentas_inactivos'] = $this->cuenta->obtenerCuentasPorEstadoModal("INACTIVA");
    $this->load->view("indicadores/cuentas", $var);
  }
  public function lecturas()
  {
    $var['lecturas_activos'] = $this->lectura->obtenerLecturasPorEstadoModal("COMPLETADO");
    $var['lecturas_inactivos'] = $this->lectura->obtenerLecturasPorEstadoModal("PENDIENTE");
    $this->load->view("indicadores/lecturas", $var);
  }
  public function lecturasC()
  {
    $var['lecturas_activos'] = $this->lectura->obtenerLecturasPorEstadoModal("COMPLETADO");
    $var['lecturas_inactivos'] = $this->lectura->obtenerLecturasPorEstadoModal("PENDIENTE");
    $this->load->view("indicadores/lecturasC", $var);
  }
  public function lecturasP()
  {
    $var['lecturas_activos'] = $this->lectura->obtenerLecturasPorEstadoModal("COMPLETADO");
    $var['lecturas_inactivos'] = $this->lectura->obtenerLecturasPorEstadoModal("PENDIENTE");
    $this->load->view("indicadores/lecturasP", $var);
  }
  public function contactos()
  {
    $var['contactos_activos'] = $this->contacto->obtenerContactosPorEstadoModal("LEIDO");
    $var['contactos_inactivos'] = $this->contacto->obtenerContactosPorEstadoModal("PENDIENTE");
    $this->load->view("indicadores/contactos", $var);
  }
}//cierre de la clase
