<script type="text/javascript" src="<?php echo base_url('script.js'); ?>"></script>
<div class="container-fluid pt-4 px-4">
    <div class="bg-light rounded p-4">
      <div class="d-flex align-items-center justify-content-between mb-4">
    <h5 class="mb-0">Listado de Clientes</h5>
</div>
      <!--Cierre de ventana-->

<div class="col-sm-12 col-xl-12">
    <div class="bg-light rounded h-100 p-2">
        <nav>
          <div class="nav nav-tabs" id="nav-tab" role="tablist">
            <button class="nav-link active" id="nav-home-tab" data-bs-toggle="tab"
            data-bs-target="#nav-home" type="button" role="tab" aria-controls="nav-home"
            aria-selected="true">Reporte Clientes</button>
            <button class="nav-link" id="nav-profile-tab" data-bs-toggle="tab"
            data-bs-target="#nav-profile" type="button" role="tab"
            aria-controls="nav-profile" aria-selected="false">Reporte de Cuentas</button>
            <button class="nav-link" id="nav-contact-tab" data-bs-toggle="tab"
            data-bs-target="#nav-contact" type="button" role="tab"
            aria-controls="nav-contact" aria-selected="false">Reporte de Lecturas</button>
          </div>
        </nav>

        <div class="tab-content pt-3" id="nav-tabContent">
            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
              <div class="container-fluid">
              <div class="card">
                  <div class="card-body">
                      <form method="POST" id="reporte_clientes_form">
                          <div class="col-12 row">
                              <div class="col-md-3">
                                  <div class="form-group">
                                      <label for="estado_cliente"><h6>Estado</h6></label>
                                      <select id="estado_cliente" class="form-control form-select" name="estado_cliente">
                                          <option selected disabled value="">SELECCIONE</option>
                                          <option value="ACTIVO">ACTIVO</option>
                                          <option value="INACTIVO">INACTIVO</option>
                                      </select>
                                  </div>
                              </div>
                              <div class="col-md-3">
                                  <div class="form-group">
                                      <label for="posee_cuenta"><h6>¿Tiene cuenta?</h6></label>
                                      <select id="posee_cuenta" class="form-control form-select" name="estado_cliente">
                                          <option selected disabled value="">SELECCIONE</option>
                                          <option value="Si">Si</option>
                                          <option value="No">No</option>
                                      </select>
                                  </div>
                              </div>
                              <div class="col-md-3">
                                  <div class="form-group">
                                      <label for="tipo"><h6>Tipo de reporte</h6></label><br>
                                      <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                          <label class="btn btn-primary">
                                              <input type="radio" name="tipo" value="excel" checked> Excel
                                          </label>
                                      </div>
                                      <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                          <label class="btn btn-primary">
                                              <input type="radio" name="tipo" value="pdf"> Pdf
                                          </label>
                                      </div>
                                  </div>
                              </div>
                              <div class="col-md-3">
                                  <div class="row">
                                      <div class="col-6">
                                        <label for="tipo"><h6>Opción</h6></label><br>
                                          <button class="btn btn-success mt-0 col-12" type="submit">Generar</button>
                                      </div>
                                  </div>
                              </div>
                          </div>
                          <div id="resultados"></div>
                      </form>
                  </div>
              </div>
              </div>
            </div>
            <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
              <div class="container-fluid">
              <div class="card mt-4">
                  <div class="card-body">
                      <form method="POST" id="reporte_cuentas_form">
                          <div class="col-12 row">
                              <div class="col-md-4">
                                  <div class="form-group">
                                      <label for="estado_cliente"><h6>Estado</h6></label>
                                      <select id="estado_cliente" class="form-control form-select" name="estado_cliente">
                                          <option selected disabled value="">SELECCIONE</option>
                                          <option value="ACTIVA">ACTIVA</option>
                                          <option value="INACTIVA">INACTIVA</option>
                                      </select>
                                  </div>
                              </div>
                              <div class="col-md-4">
                                  <div class="form-group">
                                      <label for="tipo"><h6>Tipo de reporte</h6></label><br>
                                      <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                          <label class="btn btn-primary">
                                              <input type="radio" name="tipo_cuentas" value="excel" checked> Excel
                                          </label>
                                      </div>
                                      <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                          <label class="btn btn-primary">
                                              <input type="radio" name="tipo_cuentas" value="pdf"> Pdf
                                          </label>
                                      </div>
                                  </div>
                              </div>
                              <div class="col-md-4">
                                  <div class="row">
                                      <div class="col-6">
                                        <label for="estado_cliente"><h6>Opción</h6></label>
                                          <button class="btn btn-success mt-0 col-12" type="submit">Generar</button>
                                      </div>
                                  </div>
                              </div>
                          </div>
                          <div id="resultados_cuenta"></div>
                      </form>
                  </div>
                  </div>
              </div>
            </div>
            <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
              <div class="container-fluid">
              <div class="card mt-4">
                  <div class="card-body">
                      <form method="POST" id="reporte_lecturas_form">
                          <div class="col-12 row">
                              <div class="col-md-3">
                                  <label for="fk_id_cuenta"><h6>Cliente</h6></label>
                                  <div class="row">
                                      <div class="col-10">
                                          <select class="form-select" name="fk_id_cuenta" id="fk_id_cuenta">
                                              <option value="null">SELECCIONE</option>
                                              <?php if ($listadoCuentas) : ?>
                                                  <?php foreach ($listadoCuentas->result() as $cuenta) : ?>
                                                      <option value="<?php echo $cuenta->id_cuenta; ?>">
                                                          <?php echo $cuenta->numero_medidor_cuenta; ?> -
                                                          <?php echo $cuenta->nombre_cliente; ?>
                                                          <?php echo $cuenta->apellido_cliente; ?>
                                                      </option>
                                                  <?php endforeach; ?>
                                              <?php endif; ?>
                                          </select>
                                      </div>
                                      <div class="col-2 ">
                                          <a href="javascript:reset_select();"><i class="fas fa-times mt-2"></i></a>
                                      </div>
                                  </div>

                              </div>
                              <div class="col-md-3">
                                  <div class="form-group">
                                      <label for="estado_lectura"><h6>Estado</h6></label>
                                      <select id="estado_lectura" class="form-control form-select" name="estado_lectura">
                                          <option selected disabled value="">SELECCIONE CLIENTE</option>
                                          <option value="COMPLETADO">COMPLETADO</option>
                                          <option value="PENDIENTE">PENDIENTE</option>
                                      </select>
                                  </div>
                              </div>
                              <div class="col-md-3">
                                  <div class="form-group">
                                      <label for="tipo"><h6>Tipo de reporte</h6></label><br>
                                      <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                          <label class="btn btn-primary">
                                              <input type="radio" name="tipo_lecturas" value="excel" checked> Excel
                                          </label>
                                      </div>
                                      <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                          <label class="btn btn-primary">
                                              <input type="radio" name="tipo_lecturas" value="pdf"> Pdf
                                          </label>
                                      </div>
                                  </div>
                              </div>
                              <div class="col-md-3">
                                  <div class="row">
                                      <div class="col-6">
                                        <label for="tipo"><h6>Opción</h6></label><br>
                                          <button class="btn btn-success mt-0 col-12" type="submit">Generar</button>
                                      </div>
                                  </div>
                              </div>
                          </div>
                          <div id="resultados_cuenta"></div>
                      </form>
                  </div>
                  </div>
              </div>
            </div>
          </div>
      </div>
</div>

<script type="text/javascript" src='https://code.jquery.com/jquery-3.5.1.js'></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script type="text/javascript">
    $("#fk_id_cuenta").select2({
    });
</script>
<script type="text/javascript">
    $("#reporte_clientes_form").submit(function(e) {
        e.preventDefault();
        var estado = "todos";
        var posee_cuenta = "todos";
        if ($("#estado_cliente").val()) {
            estado = $("#estado_cliente").val()
        }
        if ($("#posee_cuenta").val()) {
            posee_cuenta = $("#posee_cuenta").val()
        }
        var parametros = estado + "/" + posee_cuenta + "/" + $('input:radio[name=tipo]:checked').val();
        VentanaCentrada("<?= base_url("index.php/reportes/reporteclientes/") ?>" + parametros, 'reporte_de_clientes', '', '1024', '768', 'true');
    });

    $("#reporte_cuentas_form").submit(function(e) {
        e.preventDefault();
        var estado = "todos";
        if ($("#estado_cuenta").val()) {
            estado = $("#estado_cuenta").val()
        }
        var parametros = estado + "/" + $('input:radio[name=tipo_cuentas]:checked').val();
        VentanaCentrada("<?= base_url("index.php/reportes/reportecuentas/") ?>" + parametros, 'reporte_de_cuentas', '', '1024', '768', 'true');
    });

    $("#reporte_lecturas_form").submit(function(e) {
        e.preventDefault();
        var estado = "todos";
        var fk_id_cuenta = "";
        if ($("#estado_lectura").val()) {
            estado = $("#estado_lectura").val()
        }
        fk_id_cuenta = $("#fk_id_cuenta").val();
        var parametros = estado + "/" + $('input:radio[name=tipo_lecturas]:checked').val() + "/" + fk_id_cuenta;
        VentanaCentrada("<?= base_url("index.php/reportes/reportelecturas/") ?>" + parametros, 'reporte_de_lecturas', '', '1024', '768', 'true');
    });

    function VentanaCentrada(theURL, winName, features, myWidth, myHeight, isCenter) { //v3.0
        if (window.screen)
            if (isCenter)
                if (isCenter == "true") {
                    var myLeft = (screen.width - myWidth) / 2;
                    var myTop = (screen.height - myHeight) / 2;
                    features += (features != '') ? ',' : '';
                    features += ',left=' + myLeft + ',top=' + myTop;
                }
        window.open(theURL, winName, features + ((features != '') ? ',' : '') + 'width=' + myWidth + ',height=' + myHeight);
    }

    function reset_select() {
        $('#fk_id_cuenta').val("null").trigger('change');
    }
</script>

<!--Cierre de ventana-->
</div>
</div>
