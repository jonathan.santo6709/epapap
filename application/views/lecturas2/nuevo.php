<script type="text/javascript" src="<?php echo base_url('app.js'); ?>"></script>

<script type="text/javascript" src="<?php echo base_url('script.js'); ?>"></script>
<script>
    window.onload = function () {
      
      let encargado_lectura=document.getElementById("encargado_lectura")
      encargado_lectura.value=   localStorage.getItem("nombre_usuario")
}

function read_usuario(){

 let openRequest = indexedDB.open("epa", 1); 
 openRequest.onsuccess = function() {
  var html="";
  let db = openRequest.result;
  let transaction = db.transaction("usuario", "readonly");
      let objeto= transaction.objectStore("usuario")
      let request = objeto.openCursor() 
      request .onsuccess = (event) => {
    // report the success of our request
    const cursor = event.target.result;
    
    if (cursor) { 
  
   
  
    // cursor.value contains the current record being iterated through
    // this is where you'd do something with the result
    
    cursor.continue();
  } else {
    // no more results

  }
    
  };
}; 
}

    </script>

<div class="container-fluid pt-4 px-4">

  <div class="bg-light  rounded p-4">

    <div class="d-flex align-items-center justify-content-between mb-4">

      <h5 class="mb-0">Registro de Lecturas</h5>

    </div>



    <!--Cierre de ventana-->

    <form class="row g-3" name="f"  method="post" id="frm_nuevo_lectura">

      <div class="col-md-6">

        <label for="">

          <h6>Busqueda de cliente</h6>

        </label>

        <select class="form-select" name="fk_id_cuenta" id="fk_id_cuenta" onchange="javascript:cargar_formulario();">

          <option selected disabled value="">Seleccione el cliente</option>

          <?php if ($listadoCuentas) : ?>

            <?php foreach ($listadoCuentas->result() as $cuenta) : ?>

              <option value="<?php echo $cuenta->id_cuenta; ?>">

                <?php echo $cuenta->numero_medidor_cuenta; ?> -

                <?php echo $cuenta->nombre_cliente; ?>

                <?php echo $cuenta->apellido_cliente; ?>

              </option>

            <?php endforeach; ?>

          <?php endif; ?>

        </select>

      </div>



      <div class="col-md-6">

      <div class="form-group">

        <label for="result"><h6>Resultado</h6></label>

        <input type="text" name="result" id="result" class="form-control" value="" readonly=»readonly»>

        <input type="hidden" name="tipo_cuenta" id="tipo_cuenta" value="">

      </div>

      </div>

      <?php date_default_timezone_set('America/Guayaquil');

      $fecha_actual=date("Y-m-d H:i:s");

      ?>

      <div class="col-md-4">

        <label for="">

          <h6>Fecha Ingreso</h6>

        </label>

        <input class="form-control" type="datetime" name="fecha_lectura" id="fecha_lectura" value="<?= $fecha_actual; ?>" readonly=»readonly»>

      </div>

      <div class="col-md-4">

        <label for="">

          <h6>Lectura Anterior</h6>

        </label>

        <!-- <?php if ($lectura_actual_lectura = !" ") : ?>

          <input class="form-control" type="number" value="<?php echo $ultimaLectura->lectura_actual_lectura; ?>" name="lectura_anterior_lectura" id="lectura_anterior_lectura" placeholder="El valor anterior de la lectura es:" onchange="cal()" onkeyup="cal()" readonly=»readonly»>

        <?php else : ?>

          <input class="form-control" type="number" value="0" name="lectura_anterior_lectura" id="lectura_anterior_lectura" onchange="cal()" onkeyup="cal()" readonly=»readonly»>

        <?php endif; ?> -->

        <input class="form-control" type="number" value="0" name="lectura_anterior_lectura" id="lectura_anterior_lectura" onchange="cal()" onkeyup="cal()" readonly=»readonly»>







      </div>

      <div class="col-md-4">

        <label for="">

          <h6>Lectura Actual</h6>

        </label>

        <input class="form-control" type="number" name="lectura_actual_lectura" id="lectura_actual_lectura" placeholder="Ingrese el valor de la lectura" onchange="cal()" onkeyup="cal()" required>

      </div>

      <div class="col-md-4">

        <label for="">

          <h6>Consumo</h6>

        </label>

        <input class="form-control" type="number" name="consumo_lectura" id="consumo_lectura" readonly=»readonly» onchange="javascript:pago_estimado();">

      </div>

      <div class="col-md-4">

        <label for="">

          <h6>Pago Estimado</h6>

        </label>

        <input class="form-control" type="number" step="0.01" min="0" name="pago_lectura" id="pago_lectura" readonly=»readonly»>

      </div>

      <div class="col-md-4">

        <label for="">

          <h6>Estado</h6>

        </label>

        <select class="form-control" name="estado_lectura" id="estado_lectura" required>

          <option value="">Seleccione un estado</option>

          <option value="COMPLETADO">COMPLETADO</option>

          <option value="PENDIENTE">PENDIENTE</option>

        </select>

      </div>

      <div class="col-md-4">

        <label for="">

          <h6>Encargado lectura</h6>

        </label>

        <input class="form-control" type="text" name="encargado_lectura" id="encargado_lectura" placeholder="Ingrese el encargado de la lectura" readonly=»readonly»>

      </div>



      <div class="col-md-12">

        <label for="">

          <h6>Observación</h6>

        </label>

        <input class="form-control" type="text" name="observacion_lectura" id="observacion_lectura" placeholder="Ingrese la observación" required>

      </div>

      <div class="col-md-12">

        <button type="button" name="button" class="btn btn-primary m-2" onclick="guardar()">

          Guardar

        </button>

        <a href="<?php echo site_url(); ?>/lecturas2/index" class="btn btn-danger m-2">

          Cancelar

        </a>

      </div>

    </form>





    <script type="text/javascript">

      $("#fk_id_cuenta").select2();

    </script>



    <script type="text/javascript">

      function cal() {

        try {

          var a = parseInt(document.f.lectura_actual_lectura.value),

            b = parseInt(document.f.lectura_anterior_lectura.value);

          var total = a - b;

          if (total > 0) {

            document.f.consumo_lectura.value = a - b;

            pago_estimado()

          } else {

            document.f.consumo_lectura.value = 0;

          }



        } catch (e) {}

      }



      function pago_estimado() {

        var consumo_lectura = parseInt($("#consumo_lectura").val());

        var tipo_cuenta = $("#tipo_cuenta").val();

        var total_estimado = 0;



        if (tipo_cuenta == "DOMESTICA") {

          if (consumo_lectura <= 15) {

            total_estimado = consumo_lectura * 0.15;

          } else if (consumo_lectura >= 16 && consumo_lectura <= 30) {

            total_estimado = consumo_lectura * 0.18;

          } else if (consumo_lectura >= 31 && consumo_lectura <= 50) {

            total_estimado = consumo_lectura * 0.23;

          } else if (consumo_lectura > 50) {

            total_estimado = consumo_lectura * 0.30;

          }

        } else if (tipo_cuenta == "COMERCIAL") {

          if (consumo_lectura <= 15) {

            total_estimado = consumo_lectura * 0.59;

          } else if (consumo_lectura >= 16 && consumo_lectura <= 30) {

            total_estimado = consumo_lectura * 0.69;

          } else if (consumo_lectura >= 31 && consumo_lectura <= 50) {

            total_estimado = consumo_lectura * 0.79;

          } else if (consumo_lectura > 50) {

            total_estimado = consumo_lectura * 0.88;

          }

        } else if (tipo_cuenta == "OFICIAL") {

          if (consumo_lectura <= 15) {

            total_estimado = consumo_lectura * 0.15;

          } else if (consumo_lectura >= 16 && consumo_lectura <= 30) {

            total_estimado = consumo_lectura * 0.18;

          } else if (consumo_lectura >= 31 && consumo_lectura <= 50) {

            total_estimado = consumo_lectura * 0.23;

          } else if (consumo_lectura > 50) {

            total_estimado = consumo_lectura * 0.30;

          }

        }

        console.log(total_estimado);

        total_estimado = Math.round(total_estimado * 100) / 100

        $("#pago_lectura").val(total_estimado);

      }

    </script>

    <!--LLENAR FORMULARIO CON ELEMENTO SELECCIONADO POR SELECT-->

    <script type="text/javascript">

      function cargar_formulario() {

        const fk_id_cuenta = document.querySelector('#fk_id_cuenta');

        console.log(fk_id_cuenta)

        let valorOption = fk_id_cuenta.value;

        console.log(valorOption);



        var optionSelect = fk_id_cuenta.options[fk_id_cuenta.selectedIndex];



        console.log("Opción:", optionSelect.text);

        console.log("Valor:", optionSelect.value);



        /*Mostrando el resultado en el input*/

        inputResult = document.querySelector('#result').value = (optionSelect.text + ' - ' + optionSelect.value);



        /**Buscar Ultima Lectura */

        buscarUltimaLectura($("#fk_id_cuenta").val());

      }

      /**Buscar Ultima Lectura */

      function buscarUltimaLectura(id) {

        if (id) {

          $.ajax({

            url: "<?= base_url("index.php/lecturas/ultimaLecturaCuenta/") ?>" + id,

            success: function(data) {

              console.log(data);

              if (data) {

                $("#lectura_anterior_lectura").val(data.lectura_actual_lectura)

                $("#tipo_cuenta").val(data.nombre_tpcuenta)

              } else {

                $("#lectura_anterior_lectura").val('0')

                $("#tipo_cuenta").val('')

              }

            }

          });

        }

      }

    </script>

    <script type="text/javascript">
function guardar() { 
   
  
let fecha_lectura=document.getElementById("fecha_lectura").value
let lectura_actual_lectura=document.getElementById("lectura_actual_lectura").value
let lectura_anterior_lectura=document.getElementById("lectura_anterior_lectura").value
let consumo_lectura=document.getElementById("consumo_lectura").value
// 
let pago_lectura=document.getElementById("pago_lectura").value
let observacion_lectura=document.getElementById("observacion_lectura").value
var sel11 = document.getElementById("estado_lectura");
let estado_lectura=(sel11.options[sel11.selectedIndex].text )
let encargado_lectura=document.getElementById("encargado_lectura").value
let fk_id_cuenta=document.getElementById("fk_id_cuenta").value
var sel = document.getElementById("fk_id_cuenta");
var nombre_cliente= (sel.options[sel.selectedIndex].text).replace(/[0-9]+/g, ""); 
var numero_medidor_cuenta= (sel.options[sel.selectedIndex].text).replace(/[^0-9]+/g, ""); 
let openRequest = indexedDB.open("epa", "1");
let data={
      
      lectura_actual_lectura:lectura_actual_lectura,
      id_lectura: 0,
      nombre_cliente: nombre_cliente,
      numero_medidor_cuenta:numero_medidor_cuenta,
      lectura_anterior_lectura:lectura_anterior_lectura,
      fecha_lectura: fecha_lectura,
      consumo_lectura: consumo_lectura,
      pago_lectura:pago_lectura,
      observacion_lectura:observacion_lectura,
      estado_lectura:estado_lectura
    }
  addDB_lectura(data)
 alert("Se almaceno base de datos offline")
 setTimeout(() => {
  location.href="https://epapap.com/index.php/lecturas2/index"
 }, 3000);



}
      $("#frm_nuevo_lectura").validate({

        rules: {

          lectura_actual_lectura: {

            required: true,

            minlength: 4,

            maxlength: 4,

          },

          observación_lectura: {

            required: true

          },

          estado_lectura: {

            required: true

          },

          fk_id_cuenta: {

            required: true

          },

        },

        messages: {

          lectura_actual_lectura: {

            required: "Ingrese su lectura",

            minlength: "La lectura debe tener mínimo 4 digitos",

            maxlength: "La lectura debe tener máximo 4 digitos",

            digits: "La lectura solo acepta números"

          },

          observacion_lectura: {

            required: "Ingrese la observación"

          },

          estado_lectura: {

            required: "Por favor seleccione el estado"

          },

          fk_id_cuenta: {

            letras: "Seleccione una Cliente"

          },

        }

      });

    </script>

    <script type="text/javascript">

      function activarcaja() {

        document.getElementById('').disabled = true

      }

    </script>





    <!--Cierre de ventana-->

  </div>

</div>


<!-- 
  var correo="bryanpumax@gmail.com"
  var pass="Bryan123456*"
 let openRequest2 = indexedDB.open("epa", 1);
 openRequest2.onsuccess = function() {
  let db = openRequest2.result;
  let transaction = db.transaction("usuario", "readonly");
      let objeto= transaction.objectStore("usuario")
      let request = objeto.get(correo) 
      request .onsuccess = (event) => {
    // report the success of our request
    

    let myRecord = request.result; 
    let correo_usuario=myRecord.correo_usuario
    let password_usuario=myRecord.password_usuario
    let nombre_usuario=myRecord.nombre_usuario
    let tipo_usuario=myRecord.tipo_usuario
    alert("Ingresaste modo offline "+nombre_usuario)
    if (correo_usuario==correo && password_usuario==pass) {
      localStorage.setItem("correo_usuario",correo_usuario)
      localStorage.setItem("nombre_usuario",nombre_usuario)
      localStorage.setItem("tipo_usuario",tipo_usuario)
      window.location.href =  "https://epapap.com/index.php/indicadores2/index"; //Will take you to Google.
    }else{
      alert(" usuario y contraseña no coincide")
    }
  

  };
}; 

 -->