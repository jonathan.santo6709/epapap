    <script type="text/javascript" src="<?php echo base_url('script.js'); ?>"></script>
    <script>
    window.onload = function () {
      alert("offline lectura")
    read_lectura();
}

function read_lectura(){

 let openRequest = indexedDB.open("epa", 1);
 var tbody=document.getElementById("tbody")
 tbody.innerHTML=""
 openRequest.onsuccess = function() {
  var html="";
  let db = openRequest.result;
  let transaction = db.transaction("lectura", "readonly");
      let objeto= transaction.objectStore("lectura")
      let request = objeto.openCursor() 
      request .onsuccess = (event) => {
    // report the success of our request
    const cursor = event.target.result;
    
    if (cursor) { 
      html+='<tr>';
html+='<td>'+cursor.value.id_lectura+'</td>' ;
html+='<td>'+cursor.value.numero_medidor_cuenta+'</td>' ;
html+='<td>'+cursor.value.nombre_cliente+'</td>' ;
html+='<td>'+cursor.value.lectura_anterior_lectura+'</td>' ;
html+='<td>'+cursor.value.lectura_actual_lectura+'</td>' ;
html+='<td>'+cursor.value.fecha_lectura+'</td>' ;
html+='<td>'+cursor.value.consumo_lectura+'</td>' ;
html+='<td>'+cursor.value.pago_lectura+'</td>' ;
html+='<td>'+cursor.value.observacion_lectura+'</td>' ;
html+='<td>'+cursor.value.estado_lectura+'</td>' ;
html+='</tr>';
    // cursor.value contains the current record being iterated through
    // this is where you'd do something with the result
    
    cursor.continue();
  } else {
    // no more results

  }
   
  tbody.innerHTML=html
  };
}; 
}

    </script>

<div class="container-fluid pt-4 px-4">

    <div class="bg-light text-center rounded p-4">

      <div class="d-flex align-items-center justify-content-between mb-4">

    <h5 class="mb-0">Listado de Lecturas</h5>

    <a href="<?php echo site_url(); ?>/lecturas2/nuevo" class="btn btn-danger">

      <i class="fa fa-plus"></i>

      Nuevo Registro

    </a>

</div>



      <!--Cierre de ventana-->



  <div class="table-responsive">

  <div class="container mt-0">

  <table class="table display cellspacing="0" width="100%"" id="tbl-lecturas">

        <thead>

            <tr>

              <th class="text-center">Id</th>

              <th class="text-center">Número Medidor</th>

              <th class="text-center">Cliente</th>

              <th class="text-center">Lectura Anterior</th>

              <th class="text-center">Lectura Actual</th>

              <th class="text-center">Fecha de Lectura</th>

              <th class="text-center">Consumo</th>

              <th class="text-center">Pago</th>

              <th class="text-center">Observación</th>

              <th class="text-center">Estado</th> 

            </tr>

        </thead>

        <tbody id="tbody" name="tbody">

        </tbody>

  </table>

  </div>

  </div>






<script type="text/javascript">

    function confirmarEliminacion(id_lectura){

          iziToast.question({

              timeout: 20000,

              close: false,

              overlay: true,

              displayMode: 'once',

              id: 'question',

              zindex: 999,

              title: 'CONFIRMACIÓN',

              message: '¿Esta seguro de eliminar el lectura de forma pernante?',

              position: 'center',

              buttons: [

                  ['<button><b>SI</b></button>', function (instance, toast) {



                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');

                      window.location.href=

                      "<?php echo site_url(); ?>/lecturas/procesarEliminacion/"+id_lectura;



                  }, true],

                  ['<button>NO</button>', function (instance, toast) {



                      instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');



                  }],

              ]

          });

    }

</script>



<script type="text/javascript" src='https://code.jquery.com/jquery-3.5.1.js'></script>

<script type="text/javascript" src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script>

<script type="text/javascript" src="https://cdn.datatables.net/buttons/2.3.2/js/dataTables.buttons.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>

<script type="text/javascript" src="https://cdn.datatables.net/buttons/2.3.2/js/buttons.html5.min.js"></script>

<script type="text/javascript" src="https://cdn.datatables.net/buttons/2.3.2/js/buttons.print.min.js"></script>

<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.4.0/js/dataTables.responsive.min.js"></script>



<script type="text/javascript">

      $(document).ready(function() {

              } );

          $('#tbl-lecturas').DataTable( {

            responsive:true,

            dom: 'Blfrtip',

            buttons: [

              {

                "extend": "excelHtml5",

                "text": "<i class='fas fa-file-excel'></i>",

                "titleAttr": "Exportar a Excel",

                "className": "btn btn-success"

              },

              {

                "extend": "pdfHtml5",

                "text": "<i class='fas fa-file-pdf'></i>",

                "titleAttr": "Exportar a PDF",

                "className": "btn btn-danger"

              },

              {

                "extend": "print",

                "text": "<i class='fa fa-print'></i>",

                "titleAttr": "Imprimir",

                "className": "btn btn-info"

              },

            ],

              language: {

                  url: '<?php echo base_url(); ?>/assets/datatables/Spanish.json'

              }

          } );



</script>





<!--Cierre de ventana-->

</div>

</div>

