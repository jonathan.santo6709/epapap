<!DOCTYPE html>

<html lang="en">

   <head>

      <!-- basic -->

      <meta charset="utf-8">

      <meta http-equiv="X-UA-Compatible" content="IE=edge">

      <!-- mobile metas -->

      <meta name="viewport" content="width=device-width, initial-scale=1">

      <meta name="viewport" content="initial-scale=1, maximum-scale=1">

      <!-- site metas -->

      <title>INICIO - Bienvenido a EPAPAP</title>

      <meta name="keywords" content="">

      <meta name="description" content="">

      <meta name="author" content="">

      <link rel="manifest" href="./manifest.json">

      <!-- bootstrap css -->

      <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/publica/css/bootstrap.min.css">

      <!-- style css -->

      <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/publica/css/style.css">

      <!-- Responsive-->

      <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/publica/css/responsive.css">

      <!-- fevicon -->

      <link rel="icon" href="<?php echo base_url(); ?>/assets/publica/images/epapap.png" type="image/gif" />

      <!-- Scrollbar Custom CSS -->

      <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/publica/css/jquery.mCustomScrollbar.min.css">

      <!-- Tweaks for older IEs-->

      <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">

      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">

      <!--[if lt IE 9]>

      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>

      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->

      <!-- style boton flotante-->

      <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/publica/btn-flotante/estilos.css">

      <!--reducir texto-->

      <style media="screen">

      .review_item_text{

        display:  -webkit-box;

        -webkit-box-orient: vertical;

        -webkit-line-clamp: 2;

        line-clamp: 2;

        overflow: hidden;

      }

      .review__item__text:active{

        display: block;

      }

    </style>



    <script src="https://kit.fontawesome.com/eb496ab1a0.js" crossorigin="anonymous"></script>

    <!--Para los carruceles de noticias-->

    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/carrusel/css/swiper-bundle.min.css">

    <script type="text/javascript" src="<?php echo base_url('script.js'); ?>"></script>

    <!--fin maifest pwa-->

   </head>

   <!-- body -->

   <body class="main-layout">

     <script src="script.js"></script>

      <header>



         <div class="header">

            <div class="container">

               <div class="row">

                  <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col logo_section">

                     <div class="full">

                        <div class="center-desk">

                           <div class="logo">

                              <a href="<?php echo site_url(); ?>"><img src="<?php echo base_url(); ?>/assets/publica/images/logo-epapap.png" alt="#" /></a>

                           </div>

                        </div>

                     </div>

                  </div>

                  <div class="col-xl-10 col-lg-10 col-md-10 col-sm-10">

                     <nav class="navigation navbar navbar-expand-md navbar-dark ">

                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample04" aria-controls="navbarsExample04" aria-expanded="false" aria-label="Toggle navigation">

                        <span class="navbar-toggler-icon"></span>

                        </button>

                        <div class="collapse navbar-collapse" id="navbarsExample04">

                           <ul class="navbar-nav mr-auto">

                              <li class="nav-item active">

                                 <a class="nav-link" href="" onclick="reload()">Inicio</a>

                              </li>

                              <li class="nav-item">

                                 <a class="nav-link" href="<?php echo site_url(); ?>/misiones/listado">Nosotros</a>

                              </li>

                              <li class="nav-item">

                                 <a class="nav-link" href="<?php echo site_url(); ?>/servicios/listado">Servicios</a>

                              </li>

                              <li class="nav-item">

                                 <a class="nav-link" href="<?php echo site_url(); ?>/galerias/listado">Galería</a>

                              </li>

                              <li class="nav-item">

                                 <a class="nav-link" href="<?php echo site_url(); ?>/noticias/listado">Noticias</a>

                              </li>

                              <li class="nav-item">

                                 <a class="nav-link" href="<?php echo site_url(); ?>/contactos/listado">Contactos</a>

                              </li>

                              <li class="nav-item">

                                 <a class="nav-link"  href="<?php echo site_url(); ?>/seguridades/formularioLogin">Iniciar Sesion</a>

                              </li>

                           </ul>

                        </div>

                     </nav>

                  </div>

               </div>

            </div>

         </div>

      </header>
      <script>
function reload() {

   let openRequest = indexedDB.open("epa", "1");
openRequest.onupgradeneeded = function() {
  // se dispara si el cliente no tiene la base de datos
  // ...ejecuta la inicialización...
  console.log("creamos la base de datos bb")
  let db = openRequest.result;
  if (!db.objectStoreNames.contains('usuario')) { // si no hay un almacén de libros ("books"),
 
   db.createObjectStore('usuario', {
      keyPath: 'correo_usuario' 
  }); // crearlo 
  } 
  if (!db.objectStoreNames.contains('lectura')) { // si no hay un almacén de libros ("books"),

   db.createObjectStore('lectura', {
      keyPath: 'lectura_actual_lectura' 
  }); // crearlo 
  } 
  if (!db.objectStoreNames.contains('cliente')) { // si no hay un almacén de libros ("books"),

    db.createObjectStore('cliente', {
       keyPath: 'id_cuenta' 
   }); // crearlo 
   } 
};

openRequest.onerror = function() {
  console.error("Error bb :", openRequest.error);
};

const addDB_usuario=(datos)=>{
  let db = openRequest.result;
  let transaction = db.transaction("usuario", "readwrite");
      let objeto= transaction.objectStore("usuario")
      let request = objeto.add(datos); // (3)
}
const addDB_lectura=(datos)=>{
  let db = openRequest.result;
  let transaction = db.transaction("lectura", "readwrite");
      let objeto= transaction.objectStore("lectura")
      let request = objeto.add(datos); // (3)
}

const addDB_cliente=(datos)=>{
  let db = openRequest.result;
  let transaction = db.transaction("cliente", "readwrite");
      let objeto= transaction.objectStore("cliente")
      let request = objeto.add(datos); // (3)
}
openRequest.onsuccess = function() {

  
  let db = openRequest.result;
  if (!db.objectStoreNames.contains('usuario')) { // si no hay un almacén de libros ("books"),
 
   db.createObjectStore('usuario', {
      keyPath: 'correo_usuario' 
  }); // crearlo 
  } 
  if (!db.objectStoreNames.contains('lectura')) { // si no hay un almacén de libros ("books"),

   db.createObjectStore('lectura', {
      keyPath: 'lectura_actual_lectura' 
  }); // crearlo 
  } 
  if (!db.objectStoreNames.contains('cliente')) { // si no hay un almacén de libros ("books"),

    db.createObjectStore('cliente', {
       keyPath: 'id_cuenta' 
   }); // crearlo 
   } 
    // continúa trabajando con la base de datos usando el objeto db
    let  variable="dato=usuario"
    $.ajax({
      type: "POST",
      url: "https://epapap.com/api.php",
      data: variable,
      dataType: "JSON",
      success: function (responses) {
        var  response=(responses) 
    for (let index = 0; index < response.length; index++) {
    
      let data={
        correo_usuario: response[index].correo_usuario,
        password_usuario: response[index].password_usuario,
        id_usuario:response[index].id_usuario,
        nombre_usuario:response[index].nombre_usuario,
        tipo_usuario:response[index].tipo_usuario
      }
    addDB_usuario(data)
    }
    }
    });
    
// 
  variable="dato=lectura"
  $.ajax({
    type: "POST",
    url: "https://epapap.com/api.php",
    data: variable,
    dataType: "JSON",
    success: function (responses) {
      var  response=(responses) 
  for (let index = 0; index < response.length; index++) {
  
    let data={
      
      lectura_actual_lectura:response[index].lectura_actual_lectura,
      id_lectura: response[index].id_lectura,
      nombre_cliente: response[index].nombre_cliente,
      numero_medidor_cuenta:response[index].numero_medidor_cuenta,
      lectura_anterior_lectura:response[index].lectura_anterior_lectura,
      fecha_lectura: response[index].fecha_lectura,
      consumo_lectura: response[index].consumo_lectura,
      pago_lectura:response[index].pago_lectura,
      observacion_lectura:response[index].observacion_lectura,
      estado_lectura:response[index].estado_lectura
    }
  addDB_lectura(data)
  }
  }
  });
 
// 
variable="dato=cliente"
$.ajax({
  type: "POST",
  url: "https://epapap.com/api.php",
  data: variable,
  dataType: "JSON",
  success: function (responses) {
    var  response=(responses) 
for (let index = 0; index < response.length; index++) {

  let data={
    
    id_cuenta:response[index].id_cuenta,
    nombre_cliente: response[index].nombre_cliente,
    numero_medidor_cuenta: response[index].numero_medidor_cuenta,
    nombre_tpcuenta:response[index].nombre_tpcuenta, 
  }
addDB_cliente(data)
}
}
});
 

 
};

location.href="/"

  }

      </script>

      <!-- end header inner -->

      <!-- end header -->

