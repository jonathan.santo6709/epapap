<script type="text/javascript" src="<?php echo base_url('script.js'); ?>"></script>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <h5 class="text-success">Completadas</h5>
            <div class="list-group">
                <?php if ($lecturas_activos) : ?>
                    <?php foreach ($lecturas_activos as $u) : ?>
                        <button type="button" class="list-group-item list-group-item-action">
                          Cuenta: <?= $u->fk_id_cuenta ?> - <?= $u->encargado_lectura ?>
                        </button>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
        <div class="col-md-6">
            <h5 class="text-danger">Pendientes</h5>
            <div class="list-group">
                <?php if ($lecturas_inactivos) : ?>
                    <?php foreach ($lecturas_inactivos as $u) : ?>
                        <button type="button" class="list-group-item list-group-item-action">
                            Cuenta: <?= $u->fk_id_cuenta ?> - <?= $u->encargado_lectura ?>
                        </button>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
