<script type="text/javascript" src="<?php echo base_url('script.js'); ?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.js" ></script>
<?php
//aplicar
$ci=&get_instance();//instancia
$ci->load->model("usuario");
$ci->load->model("cliente");
$ci->load->model("cuenta");
$ci->load->model("lectura");
$ci->load->model("contacto");

$totalUsuariosActivos=0;
$totalUsuariosInactivos=0;
$totalUsuarios=0;

$totalClientesActivos=0;
$totalClientesInactivos=0;
$totalClientes=0;

$totalCuentasActivos=0;
$totalCuentasInactivos=0;
$totalCuentas=0;

$totalLecturasActivos=0;
$totalLecturasInactivos=0;
$totalLecturas=0;

$totalContactosActivos=0;
$totalContactosInactivos=0;
$totalContactos=0;

$contactosActivos=$ci->contacto->obtenerContactosPorEstado("LEIDO");
$contactosInactivos=$ci->contacto->obtenerContactosPorEstado("PENDIENTE");
if ($contactosActivos) {
  $totalContactosActivos=sizeof($contactosActivos->result());
}
if ($contactosInactivos) {
  $totalContactosInactivos=sizeof($contactosInactivos->result());
}
$totalContactos=$totalContactosActivos+$totalContactosInactivos;


$lecturasActivos=$ci->lectura->obtenerLecturasPorEstado("COMPLETADO");
$lecturasInactivos=$ci->lectura->obtenerLecturasPorEstado("PENDIENTE");
if ($lecturasActivos) {
  $totalLecturasActivos=sizeof($lecturasActivos->result());
}
if ($lecturasInactivos) {
  $totalLecturasInactivos=sizeof($lecturasInactivos->result());
}
$totalLecturas=$totalLecturasActivos+$totalLecturasInactivos;


$clientesActivos=$ci->cliente->obtenerClientesPorEstado("ACTIVO");
$clientesInactivos=$ci->cliente->obtenerClientesPorEstado("INACTIVO");
if ($clientesActivos) {
  $totalClientesActivos=sizeof($clientesActivos->result());
}
if ($clientesInactivos) {
  $totalClientesInactivos=sizeof($clientesInactivos->result());
}
$totalClientes=$totalClientesActivos+$totalClientesInactivos;


$cuentasActivos=$ci->cuenta->obtenerCuentasPorEstado("ACTIVA");
$cuentasInactivos=$ci->cuenta->obtenerCuentasPorEstado("INACTIVA");
if ($cuentasActivos) {
  $totalCuentasActivos=sizeof($cuentasActivos->result());
}
if ($cuentasInactivos) {
  $totalCuentasInactivos=sizeof($cuentasInactivos->result());
}
$totalCuentas=$totalCuentasActivos+$totalCuentasInactivos;


$usuariosActivos=$ci->usuario->obtenerUsuariosPorEstado("1");
$usuariosInactivos=$ci->usuario->obtenerUsuariosPorEstado("0");
if ($usuariosActivos) {
  $totalUsuariosActivos=sizeof($usuariosActivos->result());
}
if ($usuariosInactivos) {
  $totalUsuariosInactivos=sizeof($usuariosInactivos->result());
}
$totalUsuarios=$totalUsuariosActivos+$totalUsuariosInactivos;

?>



 <!-- MODALES -->
            <div class="modal fade" id="modal_usuarios" tabindex="-1" aria-labelledby="modal_usuariosLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h1 class="modal-title fs-5" id="modal_usuariosLabel">Usuarios</h1>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body" id="carga_usuarios">

                        </div>
                        <div class="modal-footer">

                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="modal_clientes" tabindex="-1" aria-labelledby="modal_clientesLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h1 class="modal-title fs-5" id="modal_clientesLabel">Clientes</h1>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body" id="carga_clientes">

                        </div>
                        <div class="modal-footer">

                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="modal_cuentas" tabindex="-1" aria-labelledby="modal_cuentasLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h1 class="modal-title fs-5" id="modal_cuentasLabel">Cuentas</h1>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body" id="carga_cuentas">

                        </div>
                        <div class="modal-footer">

                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="modal_lecturas" tabindex="-1" aria-labelledby="modal_lecturasLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h1 class="modal-title fs-5" id="modal_lecturasLabel">Lecturas</h1>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body" id="carga_lecturas">

                        </div>
                        <div class="modal-footer">

                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="modal_contactos" tabindex="-1" aria-labelledby="modal_contactosLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h1 class="modal-title fs-5" id="modal_contactosLabel">Contactos</h1>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body" id="carga_contactos">
                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>
                </div>
            </div>
 <!-- FIN MODALES -->
             <div class="container-fluid pt-4 px-4">
               <div class="d-flex align-items-center justify-content-between mb-4">
                   <h5 class="mb-0">Bienvenido a nuestro Sistema</h5>
               </div>
                 <div class="row g-3">
                     <div class="col-sm-6 col-xl-3">
                         <div class="bg-light rounded d-flex align-items-center justify-content-between p-4" type="button" onclick="javascript:abrir_modal()">
                             <i class="fa fa-users fa-3x text-primary"></i>
                             <div class="ms-3">
                                 <p class="mb-2">Usuarios</p>
                                 <h6 class="mb-0"><?php echo $totalUsuarios ?></h6>
                             </div>
                         </div>
                     </div>
                     <div class="col-sm-6 col-xl-3">
                         <div class="bg-light rounded d-flex align-items-center justify-content-between p-4" type="button" onclick="javascript:abrir_modal1()">
                             <i class="fa fa-users fa-3x text-primary"></i>
                             <div class="ms-3">
                                 <p class="mb-2">Clientes</p>
                                 <h6 class="mb-0"><?php echo $totalClientes ?></h6>
                             </div>
                         </div>
                     </div>
                     <div class="col-sm-6 col-xl-3">
                         <div class="bg-light rounded d-flex align-items-center justify-content-between p-4" onclick="javascript:abrir_modal2()">
                             <i class="fa fa-tachometer fa-3x text-primary"></i>
                             <div class="ms-3">
                                 <p class="mb-2">Medidores</p>
                                 <h6 class="mb-0"><?php echo $totalCuentas ?></h6>
                             </div>
                         </div>
                     </div>
                     <div class="col-sm-6 col-xl-3">
                         <div class="bg-light rounded d-flex align-items-center justify-content-between p-4" type="button" onclick="javascript:abrir_modal3()">
                             <i class="fa fa-book fa-3x text-primary"></i>
                             <div class="ms-3">
                                 <p class="mb-2">Lecturas</p>
                                 <h6 class="mb-0"><?php echo $totalLecturas ?></h6>
                             </div>
                         </div>
                     </div>
                     <div class="col-sm-6 col-xl-3">
                         <div class="bg-light rounded d-flex align-items-center justify-content-between p-4" type="button" onclick="javascript:abrir_modal4()">
                             <i class="fa fa-envelope fa-3x text-primary"></i>
                             <div class="ms-3">
                                 <p class="mb-2">Contactos</p>
                                 <h6 class="mb-0"><?php echo $totalContactos ?></h6>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>

             <?php
             $con = new mysqli("localhost","root","","epapap"); // Conectar a la BD
             $sql = "SELECT COUNT(id_usuario) AS total, estado_usuario FROM usuario
                     GROUP BY estado_usuario HAVING COUNT(id_usuario)"; // Consulta SQL
             $query = $con->query($sql); // Ejecutar la consulta SQL
             $data1 = array(); // Array donde vamos a guardar los datos
             while($r = $query->fetch_object()){ // Recorrer los resultados de Ejecutar la consulta SQL
                 $data1[]=$r; // Guardar los resultados en la variable $data
             }
             ?>

             <?php
             $con = new mysqli("localhost","root","","epapap"); // Conectar a la BD
             //$id = $_POST["Cotopaxi(Tanicuchi)"];
             $sql1 = "SELECT COUNT(id_cliente) AS total, estado_cliente FROM cliente
                     GROUP BY estado_cliente HAVING COUNT(id_cliente)"; // Consulta SQL
             $query1 = $con->query($sql1); // Ejecutar la consulta SQL
             $data = array(); // Array donde vamos a guardar los datos
             while($r1 = $query1->fetch_object()){ // Recorrer los resultados de Ejecutar la consulta SQL
                 $data2[]=$r1; // Guardar los resultados en la variable $data
             }
             ?>

             <?php
             $con = new mysqli("localhost","root","","epapap"); // Conectar a la BD
             //$id = $_POST["Cotopaxi(Tanicuchi)"];
             $sql2 = "SELECT COUNT(id_cuenta) AS total, estado_cuenta FROM cuenta
                     GROUP BY estado_cuenta HAVING COUNT(id_cuenta)"; // Consulta SQL
             $query2 = $con->query($sql2); // Ejecutar la consulta SQL
             $data = array(); // Array donde vamos a guardar los datos
             while($r2 = $query2->fetch_object()){ // Recorrer los resultados de Ejecutar la consulta SQL
                 $data3[]=$r2; // Guardar los resultados en la variable $data
             }
             ?>

             <?php
             $con = new mysqli("localhost","root","","epapap"); // Conectar a la BD
             //$id = $_POST["Cotopaxi(Tanicuchi)"];
             $sql3 = "SELECT COUNT(id_lectura) AS total, estado_lectura FROM lectura
                     GROUP BY estado_lectura HAVING COUNT(id_lectura)"; // Consulta SQL
             $query3 = $con->query($sql3); // Ejecutar la consulta SQL
             $data = array(); // Array donde vamos a guardar los datos
             while($r3 = $query3->fetch_object()){ // Recorrer los resultados de Ejecutar la consulta SQL
                 $data4[]=$r3; // Guardar los resultados en la variable $data
             }
             ?>
             <?php
              $con = new mysqli("localhost","root","","epapap"); // Conectar a la BD
              //$id = $_POST["Cotopaxi(Tanicuchi)"];
              $sql4 = "SELECT count(encargado_lectura) as total ,encargado_lectura from lectura
              group by encargado_lectura HAVING count(encargado_lectura)"; // Consulta SQL
              $query4 = $con->query($sql4); // Ejecutar la consulta SQL
              $data = array(); // Array donde vamos a guardar los datos
              while($r4 = $query4->fetch_object()){ // Recorrer los resultados de Ejecutar la consulta SQL
              $data5[]=$r4; // Guardar los resultados en la variable $data
              }
              ?>




             <!-- Sale & Revenue End -->
             <!-- Chart Start -->
             <div class="container-fluid pt-4 px-4">
                 <div class="row g-4">
                     <div class="col-sm-12 col-xl-6" type="button" onclick="javascript:abrir_modal()">
                         <div class="bg-light rounded h-100 p-4">
                             <h6 class="mb-4">Usuario por estado</h6>
                             <canvas id="chart1"></canvas>
                             <script>
                             var ctx1 = document.getElementById("chart1");
                             var data1 = {
                                     labels: [
                                     <?php foreach($data1 as $d):?>
                                     "<?php echo $d->estado_usuario?>",
                                     <?php endforeach; ?>
                                     ],
                                     datasets: [{
                                         label: 'Usuarios',
                                         data: [
                                         <?php foreach($data1 as $d):?>
                                         <?php echo $d->total;?>,
                                         <?php endforeach; ?>
                                         ],
                                         backgroundColor:[
                                           'rgb(255, 99, 132)',
                                           'rgb(54, 162, 235)',
                                           'rgb(255, 205, 86)'
                                         ],
                                         hoverOffset: 1
                                     }]
                                 };
                             var chart1 = new Chart(ctx1, {
                                 type: 'doughnut', /* valores: line, bar*/
                                 data: data1
                             });
                             </script>
                         </div>
                     </div>
                     <div class="col-sm-12 col-xl-6">
                         <div class="bg-light rounded h-100 p-4"type="button" onclick="javascript:abrir_modal1()">
                             <h6 class="mb-4">Clientes por estado</h6>
                             <canvas id="chart2"></canvas>
                             <script>
                             var ctx2 = document.getElementById("chart2");
                             var data2 = {
                                     labels: [
                                     <?php foreach($data2 as $d):?>
                                     "<?php echo $d->estado_cliente?>",
                                     <?php endforeach; ?>
                                     ],
                                     datasets: [{
                                         label: 'CLIENTES',
                                         data: [
                                     <?php foreach($data2 as $d):?>
                                     <?php echo $d->total;?>,
                                     <?php endforeach; ?>
                                         ],
                                         backgroundColor:[
                                           'rgb(54, 162, 235)',
                                           'rgb(255, 99, 132)',
                                           'rgb(255, 205, 86)'
                                         ],
                                         hoverOffset: 1
                                     }]
                                 };
                             var chart2 = new Chart(ctx2, {
                                 type: 'doughnut', /* valores: line, bar*/
                                 data: data2
                             });
                             </script>
                         </div>
                     </div>
                     <div class="col-sm-12 col-xl-6">
                         <div class="bg-light rounded h-100 p-4" onclick="javascript:abrir_modal2()">
                             <h6 class="mb-4">Cuentas por estado</h6>
                             <canvas id="chart3"></canvas>
                             <script>
                             var ctx3 = document.getElementById("chart3");
                             var data3 = {
                                     labels: [
                                     <?php foreach($data3 as $d):?>
                                     "<?php echo $d->estado_cuenta?>",
                                     <?php endforeach; ?>
                                     ],
                                     datasets: [{
                                         label: 'CUENTAS',
                                         data: [
                                     <?php foreach($data3 as $d):?>
                                     <?php echo $d->total;?>,
                                     <?php endforeach; ?>
                                         ],
                                         backgroundColor:[
                                           'rgb(255, 205, 86)',
                                           'rgb(54, 162, 235)',
                                           'rgb(255, 99, 132)'
                                         ],
                                         hoverOffset: 1
                                     }]
                                 };
                             var chart3 = new Chart(ctx3, {
                                 type: 'doughnut', /* valores: line, bar*/
                                 data: data3
                             });
                             </script>
                         </div>
                     </div>
                     <div class="col-sm-12 col-xl-6">
                         <div class="bg-light rounded h-100 p-4" type="button" onclick="javascript:abrir_modal3()">
                             <h6 class="mb-4">Lecturas por estado</h6>
                             <canvas id="chart4"></canvas>
                             <script>
                             var ctx4 = document.getElementById("chart4");
                             var data4 = {
                                     labels: [
                                     <?php foreach($data4 as $d):?>
                                     "<?php echo $d->estado_lectura?>",
                                     <?php endforeach; ?>
                                     ],
                                     datasets: [{
                                         label: 'LECTURAS',
                                         data: [
                                     <?php foreach($data4 as $d):?>
                                     <?php echo $d->total;?>,
                                     <?php endforeach; ?>
                                         ],
                                         backgroundColor:[
                                           'rgb(255, 99, 132)',
                                           'rgb(54, 162, 235)',
                                           'rgb(255, 205, 86)'
                                         ],
                                         hoverOffset: 1
                                     }]
                                 };
                             var chart3 = new Chart(ctx4, {
                                 type: 'doughnut', /* valores: line, bar*/
                                 data: data4
                             });
                             </script>
                         </div>
                     </div>
                     <div class="col-sm-12 col-xl-6">
                         <div class="bg-light rounded h-100 p-4">
                             <h6 class="mb-4">Lecturas por usuarios</h6>
                             <canvas id="chart5"></canvas>
                                                          <script>
                             var ctx5 = document.getElementById("chart5");
                             var data5 = {
                                     labels: [
                                     <?php foreach($data5 as $d):?>
                                     "<?php echo $d->encargado_lectura?>",
                                     <?php endforeach; ?>
                                     ],
                                     datasets: [{
                                         label: 'LECTURAS',
                                         data: [
                                     <?php foreach($data5 as $d):?>
                                     <?php echo $d->total;?>,
                                     <?php endforeach; ?>
                                         ],
                                         backgroundColor:[
                                           'rgb(255, 99, 132)',
                                           'rgb(54, 162, 235)',
                                           'rgb(255, 205, 86)'
                                         ],
                                         hoverOffset: 1
                                     }]
                                 };
                             var chart4 = new Chart(ctx5, {
                                 type: 'doughnut', /* valores: line, bar*/
                                 data: data5
                             });
                             </script>
                         </div>
                     </div>
                 </div>
             </div>

             <script>
                function abrir_modal() {
                    $("#modal_usuarios").modal("show");
                    $.ajax({
                        url:"<?= base_url("index.php/indicadores/usuarios") ?>",
                        success:function(data){
                            $("#carga_usuarios").html(data)
                        }
                    });
                }
             </script>
             <script>
                function abrir_modal1() {
                    $("#modal_clientes").modal("show");
                    $.ajax({
                        url:"<?= base_url("index.php/indicadores/clientes") ?>",
                        success:function(data){
                            $("#carga_clientes").html(data)
                        }
                    });
                }
             </script>
             <script>
                function abrir_modal2() {
                    $("#modal_cuentas").modal("show");
                    $.ajax({
                        url:"<?= base_url("index.php/indicadores/cuentas") ?>",
                        success:function(data){
                            $("#carga_cuentas").html(data)
                        }
                    });
                }
             </script>
             <script>
                function abrir_modal3() {
                    $("#modal_lecturas").modal("show");
                    $.ajax({
                        url:"<?= base_url("index.php/indicadores/lecturas") ?>",
                        success:function(data){
                            $("#carga_lecturas").html(data)
                        }
                    });
                }
             </script>

             <script>
                function abrir_modal4() {
                    $("#modal_contactos").modal("show");
                    $.ajax({
                        url:"<?= base_url("index.php/indicadores/contactos") ?>",
                        success:function(data){
                            $("#carga_contactos").html(data)
                        }
                    });
                }
             </script>


            <!--  Chart End -->
