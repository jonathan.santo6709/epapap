<script type="text/javascript" src="<?php echo base_url('script.js'); ?>"></script>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <h5 class="text-success">Activos</h5>
            <div class="list-group">
                <?php if ($clientes_activos) : ?>
                    <?php foreach ($clientes_activos as $u) : ?>
                        <button type="button" class="list-group-item list-group-item-action">
                            <?= $u->id_cliente ?> - <?= $u->nombre_cliente ?> <?= $u->apellido_cliente ?>
                        </button>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
        <div class="col-md-6">
            <h5 class="text-danger">Inactivos</h5>
            <div class="list-group">
                <?php if ($clientes_inactivos) : ?>
                    <?php foreach ($clientes_inactivos as $u) : ?>
                        <button type="button" class="list-group-item list-group-item-action">
                            <?= $u->id_cliente ?> - <?= $u->nombre_cliente ?> <?= $u->apellido_cliente ?>
                        </button>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
