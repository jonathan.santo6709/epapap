<script type="text/javascript" src="<?php echo base_url('script.js'); ?>"></script>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <h5 class="text-success">Activos</h5>
            <div class="list-group">
                <?php if ($contactos_activos) : ?>
                    <?php foreach ($contactos_activos as $u) : ?>
                        <button type="button" class="list-group-item list-group-item-action">
                            <?= $u->nombre_contacto ?> <?= $u->apellido_contacto ?>
                        </button>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
        <div class="col-md-6">
            <h5 class="text-danger">Inactivos</h5>
            <div class="list-group">
                <?php if ($contactos_inactivos) : ?>
                    <?php foreach ($contactos_inactivos as $u) : ?>
                        <button type="button" class="list-group-item list-group-item-action">
                           <?= $u->nombre_contacto ?> <?= $u->apellido_contacto ?>
                        </button>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
