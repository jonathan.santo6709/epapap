<script type="text/javascript" src="<?php echo base_url('script.js'); ?>"></script>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <h5 class="text-success">Activos</h5>
            <div class="list-group">
                <?php if ($cuentas_activos) : ?>
                    <?php foreach ($cuentas_activos as $u) : ?>
                        <button type="button" class="list-group-item list-group-item-action">
                          Cliente: <?= $u->fk_id_cliente ?> - <?= $u->numero_cuenta ?> - <?= $u->numero_medidor_cuenta ?>
                        </button>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
        <div class="col-md-6">
            <h5 class="text-danger">Inactivos</h5>
            <div class="list-group">
                <?php if ($cuentas_inactivos) : ?>
                    <?php foreach ($cuentas_inactivos as $u) : ?>
                        <button type="button" class="list-group-item list-group-item-action">
                            Cliente: <?= $u->fk_id_cliente ?> - <?= $u->numero_cuenta ?> - <?= $u->numero_medidor_cuenta ?>>
                        </button>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
