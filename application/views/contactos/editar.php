<script type="text/javascript" src="<?php echo base_url('script.js'); ?>"></script>
<div class="container-fluid pt-4 px-4">
    <div class="bg-light rounded p-4">
      <div class="d-flex align-items-center justify-content-between mb-4">
          <h5 class="mb-0">Edición de contactos</h5>
      </div>
      <!--Cierre de ventana-->

<form class="row g-3" action="<?php echo site_url(); ?>/contactos/procesarActualizacion" method="post" enctype="multipart/form-data">
<input type="hidden" name="id_contacto" id="id_contacto"value="<?php echo $contacto->id_contacto; ?>">
<div class="col-md-4 ">
  <label for=""><h6>Nombres y Apellidos</h6></label>
   <input class="form-control" placeholder="Ingrese el nombre" type="text" name="nombre_contacto" id="nombre_contacto" value="<?php echo $contacto->nombre_contacto; ?> <?php echo $contacto->apellido_contacto; ?>" required readonly=»readonly»>
</div>
<div class="col-md-4">
  <label for=""><h6>Correo</h6></label>
   <input class="form-control" placeholder="Ingrese el correo" type="email" name="correo_contacto" id="correo_contacto" value="<?php echo $contacto->correo_contacto; ?>" required readonly=»readonly»>
</div>
<div class="col-md-4">
  <label for=""><h6>Teléfono</h6></label>
   <input class="form-control" placeholder="Ingrese el Teléfono" type="number" name="telefono_contacto" id="telefono_contacto" value="<?php echo $contacto->telefono_contacto; ?>" required readonly=»readonly»>
</div>
<div class="col-md-4">
  <label for=""><h6>Estado</h6></label>
  <select class="form-control" name="estado_contacto" id="estado_contacto">
    <option selected disabled value="">Seleccione un estado</option>
    <option value="PENDIENTE">PENDIENTE</option>
    <option value="LEIDO">LEIDO</option>
  </select>
</div>
<div class="col-md-4">
  <label for=""><h6>Fecha Ingreso</h6></label>
  <input class="form-control" type="datetime" name="fecha_ingreso_contacto" id="fecha_ingreso_contacto" value="<?php echo $contacto->fecha_ingreso_contacto; ?>" readonly=»readonly»>
</div>
<?php date_default_timezone_set('America/Guayaquil');
$fecha_actual=date("Y-m-d H:i:s");
?>
<div class="col-md-4">
  <label for=""><h6>Fecha Actualización</h6></label>
  <input class="form-control" type="datetime" name="fecha_actualizacion_contacto" id="fecha_actualizacion_contacto" value="<?= $fecha_actual; ?>" readonly=»readonly»>
</div>
<div class="col-md-12">
   <textarea class="form-control" placeholder="Ingrese el mensaje" type="text" name="mensaje_contacto" id="mensaje_contacto" Message="Name" readonly=»readonly»><?php echo $contacto->mensaje_contacto; ?></textarea>
</div>
<div class="col-md-4">
  <button type="submit" name="button" class="btn btn-primary m-2">
    Guardar
  </button>
  <a href="<?php echo site_url(); ?>/contactos/index"
    class="btn btn-danger m-2">
    Cancelar
  </a>
</div>
  </form>
  <script type="text/javascript">
    $('#estado_contacto').val('<?php echo $contacto->estado_contacto; ?>');
  </script>
<!--Cierre de ventana-->
</div>
</div>
