const CACHE_NAME = 'epapap-v1';

self.addEventListener('fetch', event => {
  event.respondWith(
    fetch(event.request)
      .then(response => {
        // Clonar la respuesta para que podamos guardarla en caché
        const responseClone = response.clone();

        caches.open(CACHE_NAME)
          .then(cache => {
            cache.put(event.request, responseClone);
          });

        return response;
      })
      .catch(() => {
        return caches.match(event.request)
          .then(response => {
            return response || new Response(null, {status: 404});
          });
      })
  );
});



/* 
 */
const staticDevCoffee = CACHE_NAME
const assets = [
  "https://epapap.com/",
  "https://epapap.com/index.php/misiones/listado",
  "https://epapap.com/index.php",
  "https://epapap.com//assets/publica/css/bootstrap.min.css",
  "https://epapap.com//assets/publica/css/style.css",
  "https://epapap.com//assets/publica/css/responsive.css",
  "https://epapap.com//assets/publica/images/epapap.png",
  "https://epapap.com//assets/publica/css/jquery.mCustomScrollbar.min.css",
  "https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css",
  "https://epapap.com//assets/publica/btn-flotante/estilos.css",
  "https://kit.fontawesome.com/eb496ab1a0.js",
  "https://epapap.com/index.php/seguridades/formularioLogin",
  "https://epapap.com/index.php/seguridades/validarAcceso",
  "https://epapap.com/script.js",
  "https://epapap.com/bdbb.js",
  "https://epapap.com/index.php/indicadores2/index",
  "https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js",
  "https://epapap.com//uploads/usuarios/foto_usuario_1677275786_364.png",
  "https://epapap.com/index.php/lecturas2/index",
  "https://epapap.com/index.php/clientes/index",
  "https://epapap.com/index.php/sectores/index",
  "https://epapap.com/index.php/cuentas/index",
  "https://epapap.com/index.php/tpcuentas/index",
  "https://epapap.com/index.php/lecturas2/nuevo"


]

self.addEventListener("install", installEvent => {
  installEvent.waitUntil(
    caches.open(staticDevCoffee).then(cache => {
      cache.addAll(assets)
    })
  )
})


