
let name="epa"; 
let openRequest = indexedDB.open("epa", "1");
openRequest.onupgradeneeded = function() {
  // se dispara si el cliente no tiene la base de datos
  // ...ejecuta la inicialización...
  console.log("creamos la base de datos bb")
  let db = openRequest.result;
  if (!db.objectStoreNames.contains('usuario')) { // si no hay un almacén de libros ("books"),
 
   db.createObjectStore('usuario', {
      keyPath: 'correo_usuario' 
  }); // crearlo 
  } 
  if (!db.objectStoreNames.contains('lectura')) { // si no hay un almacén de libros ("books"),

   db.createObjectStore('lectura', {
      keyPath: 'lectura_actual_lectura' 
  }); // crearlo 
  } 
  if (!db.objectStoreNames.contains('cliente')) { // si no hay un almacén de libros ("books"),

    db.createObjectStore('cliente', {
       keyPath: 'id_cuenta' 
   }); // crearlo 
   } 
};

openRequest.onerror = function() {
  console.error("Error bb :", openRequest.error);
};

const addDB_usuario=(datos)=>{
  let db = openRequest.result;
  let transaction = db.transaction("usuario", "readwrite");
      let objeto= transaction.objectStore("usuario")
      let request = objeto.add(datos); // (3)
}
const addDB_lectura=(datos)=>{
  let db = openRequest.result;
  let transaction = db.transaction("lectura", "readwrite");
      let objeto= transaction.objectStore("lectura")
      let request = objeto.add(datos); // (3)
}

const addDB_cliente=(datos)=>{
  let db = openRequest.result;
  let transaction = db.transaction("cliente", "readwrite");
      let objeto= transaction.objectStore("cliente")
      let request = objeto.add(datos); // (3)
}
openRequest.onsuccess = function() {

  
  let db = openRequest.result;
  if (!db.objectStoreNames.contains('usuario')) { // si no hay un almacén de libros ("books"),
 
   db.createObjectStore('usuario', {
      keyPath: 'correo_usuario' 
  }); // crearlo 
  } 
  if (!db.objectStoreNames.contains('lectura')) { // si no hay un almacén de libros ("books"),

   db.createObjectStore('lectura', {
      keyPath: 'lectura_actual_lectura' 
  }); // crearlo 
  } 
  if (!db.objectStoreNames.contains('cliente')) { // si no hay un almacén de libros ("books"),

    db.createObjectStore('cliente', {
       keyPath: 'id_cuenta' 
   }); // crearlo 
   } 
    // continúa trabajando con la base de datos usando el objeto db
    let  variable="dato=usuario"
    $.ajax({
      type: "POST",
      url: "https://epapap.com/api.php",
      data: variable,
      dataType: "JSON",
      success: function (responses) {
        var  response=(responses) 
    for (let index = 0; index < response.length; index++) {
    
      let data={
        correo_usuario: response[index].correo_usuario,
        password_usuario: response[index].password_usuario,
        id_usuario:response[index].id_usuario,
        nombre_usuario:response[index].nombre_usuario,
        tipo_usuario:response[index].tipo_usuario
      }
    addDB_usuario(data)
    }
    }
    });
    
// 
  variable="dato=lectura"
  $.ajax({
    type: "POST",
    url: "https://epapap.com/api.php",
    data: variable,
    dataType: "JSON",
    success: function (responses) {
      var  response=(responses) 
  for (let index = 0; index < response.length; index++) {
  
    let data={
      
      lectura_actual_lectura:response[index].lectura_actual_lectura,
      id_lectura: response[index].id_lectura,
      nombre_cliente: response[index].nombre_cliente,
      numero_medidor_cuenta:response[index].numero_medidor_cuenta,
      lectura_anterior_lectura:response[index].lectura_anterior_lectura,
      fecha_lectura: response[index].fecha_lectura,
      consumo_lectura: response[index].consumo_lectura,
      pago_lectura:response[index].pago_lectura,
      observacion_lectura:response[index].observacion_lectura,
      estado_lectura:response[index].estado_lectura
    }
  addDB_lectura(data)
  }
  }
  });
 
// 
variable="dato=cliente"
$.ajax({
  type: "POST",
  url: "https://epapap.com/api.php",
  data: variable,
  dataType: "JSON",
  success: function (responses) {
    var  response=(responses) 
for (let index = 0; index < response.length; index++) {

  let data={
    
    id_cuenta:response[index].id_cuenta,
    nombre_cliente: response[index].nombre_cliente,
    numero_medidor_cuenta: response[index].numero_medidor_cuenta,
    nombre_tpcuenta:response[index].nombre_tpcuenta, 
  }
addDB_cliente(data)
}
}
});
 

 
};


