

// DOM Elements
const lecturaForm = document.getElementById("frm_nuevo_lectura");
const lecturaContainer = document.querySelector(".lectura");
const fk_id_cuentaInput = frm_nuevo_lectura["fk_id_cuenta"];
const fecha_lecturaInput = frm_nuevo_lectura["fecha_lectura"];
const lectura_anterior_lecturaInput = frm_nuevo_lectura["lectura_anterior_lectura"];


const lectura = JSON.parse(localStorage.getItem("lectura")) || [];

const addLectura = (fk_id_cuenta, fecha_lectura, lectura_anterior_lectura) => {
  lectura.push({
    fk_id_cuenta,
    fecha_lectura,
    lectura_anterior_lectura,
  });

  localStorage.setItem("lectura", JSON.stringify(lectura));

  return { fk_id_cuenta, fecha_lectura, lectura_anterior_lectura };
};

const createLecturaElement = ({ fk_id_cuenta, fecha_lectura, lectura_anterior_lectura }) => {
  // Create elements
  const lecturaDiv = document.createElement("div");
  const lecturafk_id_cuenta = document.createElement("h2");
  const lecturafecha_lectura = document.createElement("p");
  const lecturalectura_anterior_lectura = document.createElement("p");

  // Fill the content
  lecturafk_id_cuenta.innerText = "Cuenta: " + fk_id_cuenta;
  lecturafecha_lectura.innerText = "Fecha: " + fecha_lectura;
  lecturalectura_anterior_lectura.innerText = "lectura: " + lectura_anterior_lectura;

  // Add to the DOM
  lecturaDiv.append(lecturafk_id_cuenta, lecturafecha_lectura, lecturalectura_anterior_lectura);
  lecturaContainer.appendChild(lecturaDiv);

  lecturaContainer.style.display = lectura.length === 0 ? "none" : "flex";
};

lecturaContainer.style.display = lectura.length === 0 ? "none" : "flex";

lectura.forEach(createLecturaElement);

lecturaForm.onsubmit = e => {
  e.preventDefault();

  const newLectura = addLectura(
    fk_id_cuentaInput.value,
    fecha_lecturaInput.value,
    lectura_anterior_lecturaInput.value
  );

  createLecturaElement(newLectura);

  fk_id_cuentaInput.value = "";
  fecha_lecturaInput.value = "";
  lectura_anterior_lecturaInput.value = "";
};

