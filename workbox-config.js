module.exports = {
	globDirectory: '.',
	globPatterns: [
		'**/*.{html,php,json,md,afm,ttf,ufm,ser,dist,GPL,xml,png,svg,css,LGPL,htm,txt,TXT,xlsx,js,jpg,gif,scss,eot,woff,woff2,otf,sql,rst,xls}'
	],
	swDest: 'C:/xampp/htdocs/empresa/sw.js',
	ignoreURLParametersMatching: [
		/^utm_/,
		/^fbclid$/
	]
};
