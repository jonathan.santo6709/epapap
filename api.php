<?php
ini_set('display_errors', 1); ini_set('display_startup_errors', 1); error_reporting(E_ALL);
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS');
header("Access-Control-Allow-Headers: X-Requested-With");
header('Content-Type: text/html; charset=utf-8');
header('P3P: CP="IDC DSP COR CURa ADMa OUR IND PHY ONL COM STA"');

function consultas($tabla, $campo, $texto_condicion)
{
    require "conexionbdd.php";
    $sentecia = "SELECT $campo from $tabla $texto_condicion;";
    $sql = $pdo->query($sentecia);
    return $sql;
}

function update_table($tabla, $campo_valor, $texto)
{
    require "conexionbdd.php";
    $sentecia = "UPDATE $tabla SET $campo_valor   $texto";

    $sql = $pdo->query($sentecia);
    return $sql;
}

function insertar($tabla, $campo, $valor)
{
    include("conexionbdd.php");
    $sentecia = "INSERT INTO  $tabla ($campo) values ($valor);";
    $sql = $pdo->query($sentecia);
    return $sql;
}
function operacion($texto)
{
    require "conexionbdd.php";
    $sentecia = " $texto;";

    $sql = $pdo->query($sentecia);
    return $sql;
}
function insertarP($tabla, $campo, $valor)
{
    $sentecia = "INSERT INTO  $tabla ($campo) values ($valor);";

    return $sentecia;
}
$dato = $_REQUEST["dato"];
switch ($dato) {
    case 'usuario':
echo usuario();
// echo "Hola";
        break;
case 'lectura':
    echo lectura();
    break;
    case 'cliente':
        # code...
        echo cliente_medidor();
        break;
}

function usuario()
{
    
    $campo = "id_usuario, nombre_usuario, apellido_usuario, telefono_usuario, correo_usuario, password_usuario, descripcion_usuario, estado_usuario, foto_usuario, fecha_ingreso_usuario, fecha_actualizacion_usuario, tipo_usuario, fk_id_sector ";
        $where = "";
        $tabla = "usuario";
        $inner = " ";
        $lista_array = array();
        $sql = consultas("$tabla", "$campo", "$where $inner ");
        
        while ($row = $sql->fetch()) { 
           
    
            array_push($lista_array, array("id_usuario" => $row["id_usuario"],"nombre_usuario"=>$row["nombre_usuario"]." ".$row["apellido_usuario"],"telefono_usuario"=>$row["telefono_usuario"],
            "correo_usuario"=>$row["correo_usuario"],
            "password_usuario"=>$row["password_usuario"],
            "descripcion_usuario"=>$row["descripcion_usuario"],
            "estado_usuario"=>$row["estado_usuario"],
            "foto_usuario"=>$row["foto_usuario"],
            "tipo_usuario"=>$row["tipo_usuario"]
        ));
        }
    
        return json_encode($lista_array);
}
function cliente_medidor(){
    $campo = "id_cuenta,numero_medidor_cuenta,cliente.nombre_cliente ,cliente.apellido_cliente,tpcuenta.nombre_tpcuenta";
    $where = "";
    $tabla = "cuenta";
    $inner = " INNER JOIN cliente on cliente.id_cliente=cuenta.fk_id_cliente INNER JOIN tpcuenta on tpcuenta.id_tpcuenta=cuenta.fk_id_tpcuenta ";
    $lista_array = array();
    $sql = consultas("$tabla", "$campo", "$where $inner ");
    
    while ($row = $sql->fetch()) { 
       

        array_push($lista_array, array(
            "id_cuenta" => $row["id_cuenta"],
            "nombre_cliente"=>$row["nombre_cliente"]." ".$row["apellido_cliente"],
            "numero_medidor_cuenta"=>$row["numero_medidor_cuenta"],
             "nombre_tpcuenta"=>$row["nombre_tpcuenta"] 
    ));
    }

    return json_encode($lista_array);
}

function lectura(){
    $campo = "`id_lectura`, `fecha_lectura`, `lectura_anterior_lectura`, `lectura_actual_lectura`, `consumo_lectura`, `pago_lectura`, `observacion_lectura`, `fecha_actualizacion_lectura`, `estado_lectura`, `encargado_lectura`, `fk_id_cuenta`,`id_cuenta`, `numero_cuenta`, `numero_medidor_cuenta`, `direccion_primaria_cuenta`, `direccion_secundaria_cuenta`, `estado_cuenta`, `fecha_ingreso_cuenta`, `fecha_actualizacion_cuenta`, `fk_id_cliente`, `fk_id_sector`, `fk_id_tpcuenta`, `id_cliente`, `cedula_cliente`, `nombre_cliente`, `apellido_cliente`, `telefono_cliente`, `correo_cliente`, `estado_cliente`, `fecha_ingreso_cliente`, `fecha_actualizacion_cliente`";
    $where = "";
    $tabla = "lectura";
    $inner = " INNER JOIN cuenta on cuenta.id_cuenta=lectura.fk_id_cuenta INNER JOIN cliente on cliente.id_cliente=cuenta.fk_id_cliente";
    $lista_array = array();
    $sql = consultas("$tabla", "$campo", "$where $inner ");
    if ($sql->rowCount()==0) {
        array_push($lista_array, array(
            "id_lectura" => "-",
            "nombre_cliente"=>"-",
            "numero_medidor_cuenta"=>"-",
        "lectura_anterior_lectura"=>"-",
        "lectura_actual_lectura"=>"-",
        "fecha_lectura"=>"-",
        "consumo_lectura"=>"-",
        "pago_lectura"=>"-",
        "observacion_lectura"=>"-",
        "estado_lectura"=>"-"
    ));
    }
    while ($row = $sql->fetch()) { 
       

        array_push($lista_array, array(
            "id_lectura" => $row["id_lectura"],
            "nombre_cliente"=>$row["nombre_cliente"]." ".$row["apellido_cliente"],
            "numero_medidor_cuenta"=>$row["numero_medidor_cuenta"],
        "lectura_anterior_lectura"=>$row["lectura_anterior_lectura"],
        "lectura_actual_lectura"=>$row["lectura_actual_lectura"],
        "fecha_lectura"=>$row["fecha_lectura"],
        "consumo_lectura"=>$row["consumo_lectura"],
        "pago_lectura"=>$row["pago_lectura"],
        "observacion_lectura"=>$row["observacion_lectura"],
        "estado_lectura"=>$row["estado_lectura"]
    ));
    }

    return json_encode($lista_array);
}